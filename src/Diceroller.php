<?php

namespace AthenaTT\Diceroller;

class Diceroller
{
    private $input = '';
    private $rolls = [];

    // 1d20
    function __construct($string)
    {
        $this->input = $string;
        $this->run();
    }

    public function format()
    {
        return $this->input;
    }

    public function getRolls() {
        return $this->rolls;
    }

    public function run()
    {
        $this->input = $this->parseRoll($this->input);
    }

    public function parseRoll($message)
    {
        $count = 0;
        $message = preg_replace_callback(
            // 1d20
            '/([\d]*?)d([\d]*)/',
            function ($matches) {
                return $this->roll($matches[2], $matches[1]);
            },
            $message,
            -1,
            $count,
            PREG_UNMATCHED_AS_NULL 
        );
        return $message;
    }

    public function roll($dice, $amount)
    {
        $result = 0;
        if ($amount == null) $amount = 1;
        \Debugbar::info("-$amount-");
        for ($t = 0; $t < $amount; $t++) {
            $roll = rand(1,$dice);
            $this->rolls[] = $roll;
            $result += $roll;
        }
        return $result;
    }
}
